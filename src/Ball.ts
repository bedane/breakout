import {Breakout} from './Breakout';
import {SignalManager} from './manager/SignalManager';

export class Ball extends Phaser.Graphics{

    private _ball: Phaser.Graphics = null;

    get sprite(): Phaser.Graphics {
        return this._ball;
    }

    constructor() {
        super(Breakout.instance, 0, 0);

        this._init();

    }

    private _init(): void {

        this.beginFill(0x000000, 1);
        this.drawCircle( 0, 0, 30);
            
        this.game.physics.arcade.enable(this);
        this.body.collideWorldBounds = true;
        this.body.bounce.setTo(1, 1.05);

    }

    public show(delay):void {
        var tween = this.game.add.tween(this).to({alpha: 1}, 800, 'Linear', true, delay );
        tween.onComplete.add( ()=>{
            SignalManager.instance.dispatch('ready');
        })
    }

    public getGoing():void {

        this.body.velocity.x = this.game.rnd.integerInRange(-50, 50);
        this.body.velocity.y = this.game.rnd.integerInRange(220, 300);
    
    }
}