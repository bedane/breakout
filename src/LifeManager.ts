import {GameGroup} from './util/GameGroup';
export class LifeManager extends GameGroup {

    private _lives:number = null;
    private _livesArray: Phaser.Graphics[] = []

    constructor(lives: number) {
        super();

        this._lives = lives;
        
        for ( let i:number = 0; i<lives; i++ ){

            // create a heart
            var graphic: Phaser.Graphics = this.game.add.graphics(0, 0, this);
            graphic.beginFill(0x000000, 1);
            graphic.drawCircle( 0, 0, 30);

            graphic.position.x = i * 40;

            this._livesArray.push(graphic);
        }

    }

    /**
     *  @name:      lifeLost
     *  @desc:      remove life, decrement the number and pop a graphic and destroy from the the life array
     */
    public lifeLost(): number {

        this._lives--;
        var graphic = this._livesArray.pop()
        graphic.destroy();

        return this._lives;
    }


}