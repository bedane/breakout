import {Signal} from './../util/Signal';
export class SignalManager {

     // Stores the instance of SignalManager.
     public static instance:     SignalManager   = null;
     
     // Stores all currently active Signals.
     private _signalArray:       Signal[]        = null;

    /**
     * @name:                       SignalManager
     * @description:                Creates a new instance of signalmanager
     * @params:                     void
     * @return:                     void
     */
    public constructor() {
        if (SignalManager.instance != null) {
            // If this is fired, you tried to make 2 SignalManagers, which is wrong.
            throw new Error("Tried to make a signal manager when one already existed. To use SignalManager, use SignalManager.instance.");
        } else {
            // Creates and adds singleton instance.
            SignalManager.instance = this;  
            this._signalArray = [];
                            
        }
    };

    /**
     * @name:                       add
     * @description:                Creates a new signal if the ID doesn't exist, if not adds a function to the existing signal.
     * @params:                     ID: string, Listener: functon, Context: 'this', priority: number, args: ...any
     * @return:                     boolean - true if a new signal is created. false if the signal already existed.
     */
    public add(addID: string, listener: Function, listeningContext: any = null, priority: number = 0, ...args: any[]): boolean {
        var added: boolean = false;
        this._signalArray.forEach( signal => {
            if (signal.ID === addID) {
                signal.add(listener, listeningContext, priority, args);
                added = true;
            } 
        });
        if (!added) {
            var newSignal: Signal = new Signal(addID);
            this._signalArray.push(newSignal);
            newSignal.add(listener, listeningContext, priority, ...args);
            return true;
        } else {
            return false;
        }
    };
    
    /**
     * @name:                       remove
     * @description:                Removes a signal from the signal manager.
     * @params:                     ID: string, listener: function, context: scope
     * @return:                     boolean: true = removed. false = not removed.
     */
    public remove(removeID: string, listener: Function, listeningContext: any = null): boolean {
        this._signalArray.forEach( signal => {
            if (signal.ID === removeID) {
                signal.remove(listener, listeningContext);
                return true;
            } 
        });
        
        return false;
    };
    
    /**
     * @name:                       dispatch
     * @description:                Dispatches a signal event with the given ID.
     * @params:                     ID: string, params: ...any
     * @return:                     boolean: true = fired. false = didnt exist.
     */
    public dispatch(dispatchID: string, ...params: any[]): boolean {
        this._signalArray.forEach( signal => {
            if (signal.ID === dispatchID) {
                signal.dispatch(...params);
                return true;
            } 
        });
        return false;
    };
        
}