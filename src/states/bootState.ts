import {BlockMap} from './../maps/blockMap';
/**
 *  bootState is used to setup the canvas for production to include
 *      - scaling the canvas to screen fit
 *      - preloading assets
 * 
 */
export class bootState extends Phaser.State {


    preload() {
        this.game.load.audio('click1', '/assets/sounds/click1.wav');
        this.game.load.audio('click2', '/assets/sounds/click2.wav');
        this.game.load.audio('click3', '/assets/sounds/click3.wav');
        this.game.load.audio('wallBounce', '/assets/sounds/wallBounce.wav');

        new BlockMap();
        
        this._createMaps();

    }

    create() {  

        //remove margin on body
        document.body.style.margin = '0';
        document.body.style.backgroundColor = "#ffb347"; 
        this.game.stage.backgroundColor = "#ffb347";
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        this.game.state.start('mainGame');

    }

    resize() {

        

    }

    private _createMaps(): void {

        var map1 = [
            [50, 300],
            [160, 300],
            [270, 300],
            [380, 300],
            [490, 300],
            [50, 330],
            [160, 330],
            [270, 330],
            [380, 330],
            [490, 330],
            [50, 360],
            [160, 360],
            [270, 360],
            [380, 360],
            [490, 360],
            [50, 390],
            [160, 390],
            [270, 390],
            [380, 390],
            [490, 390],
        ]

        BlockMap.instance.addMap(map1);

    } // end _createMaps

}
