import {Splash} from './../scenes/Splash';
import {MainGame} from './../scenes/MainGame';
import {EndGame} from './../scenes/EndGame';
import {Blob} from './../Blob';
import {Block} from './../Block';
import {Paddle} from './../Paddle';
import {Ball} from './../Ball';
import {SignalManager} from './../manager/SignalManager';

export class mainGameState extends Phaser.State {

    private _background         = null;
    private _splash: Splash     = null;
    private _mainGame: MainGame = null;
    private _endGame: EndGame   = null;

    create() {

        var group = new Phaser.Group(this.game);
        this._background = group;

        this._createBackgrondBlobs();
        
        this._makeGame();

        // set arcade physics on
        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        SignalManager.instance.add('newGame', this._makeGame, this);

    } // end Create


    private _makeGame(): void {
        this._endGame   = new EndGame();
        this._mainGame  = new MainGame();
        this._splash    = new Splash();
    }

    /**
     *  @name:          _createBackgroundBlobs
     *  @description:   creates background blobs that move in the game area 
     *  @param:
     *  @return:        null
     */
    private _createBackgrondBlobs():void {
        // create background blobs
        for ( let i = 0; i < 10; i++ ){
            
            var blob: Blob = new Blob();
            this._background.add(blob);
        } // end blob loop

    } // end _createBackgroundBlobs

}