import {Breakout} from './Breakout';

enum Color {
    Red = 'ffe4e1', 
    Green = 'f0fff0', 
    Blue = 'f0f8ff', 
    Yellow = 'fffacd', 
    Orange = 'ffdead', 
    Purple = 'e6e6fa'
}

export class Blob extends Phaser.Group {

    private _color = [Color.Blue, Color.Green, Color.Blue, Color.Yellow, Color.Orange, Color.Purple ];

    constructor() {
        super(Breakout.instance);
        this._init();
    }

    private _init(): void {

        var color:any   = '0x' + this._color[this.game.rnd.integerInRange(0,5)];

        var graphics    = this.game.add.graphics(0, 0);
        graphics.beginFill(color, 1);
        graphics.drawCircle(this.game.rnd.integerInRange(0, this.game.width),this.game.rnd.integerInRange(0, this.game.height), this.game.rnd.integerInRange(100, 400));
        graphics.alpha = this.game.rnd.realInRange(0, 0.6);

        this.add(graphics);

        // now move them
        this.game.add.tween( graphics).to( {
            x: this.game.rnd.integerInRange( -200, 200),
            y: this.game.rnd.integerInRange( -200, 200)
        }, this.game.rnd.integerInRange(10000, 20000), 'Linear', true, this.game.rnd.integerInRange(0, 5000), -1, true);

        this.game.add.tween(graphics.scale).to({
            x: 0.8,
            y: 0.8
        }, this.game.rnd.integerInRange(10000, 20000), 'Linear', true, this.game.rnd.integerInRange(0, 5000), -1, true);

    }

}