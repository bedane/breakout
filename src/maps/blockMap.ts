export class BlockMap {

    public static instance: BlockMap = null;
    private _blockMapArray = [];

    get blockMapArray() {
        return this._blockMapArray;
    }

    public constructor(){
        if (BlockMap.instance != null) {
            // If this is fired, you tried to make 2 SignalManagers, which is wrong.
            throw new Error("Tried to make a signal manager when one already existed. To use SignalManager, use SignalManager.instance.");
        } else {
            // Creates and adds singleton instance.
            BlockMap.instance = this;
                            
        }
    } 

    public addMap(array): void { 
        this._blockMapArray.push(array);
    }

    public getRandomMap() {
        return this._blockMapArray[0]; //[ Math.floor( Math.random() * (this._blockMapArray.length - 0 + 1)) + 0 ]
    }

}