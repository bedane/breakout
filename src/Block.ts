// import {GameGroup} from './util/GameGroup';
import {Breakout} from './Breakout';
import {SignalManager} from './manager/SignalManager';

export class Block extends Phaser.Graphics{

    private _width: number = null;
    private _height: number = null;
    private _block: Phaser.Graphics = null;

    get sprite(): Phaser.Graphics {
        return this._block;
    }

    constructor(dimensions: Phaser.Point) {
        
        super(Breakout.instance, 0, 0);

        this._width     = dimensions.x;
        this._height    = dimensions.y;

        this._init();

    }

    /**
     *  @name:          _init
     *  @description:   initialises the class and draws the block based on the size passed in
     *  @param:         null
     *  @return:        null
     */
    private _init(): void {

        this.lineStyle(4, 0x000000, 1);
        this.drawRect(0, 0, 100, 20);
        
        this.pivot.x = 0.5;
        this.pivot.y = 0.5;

        this.game.physics.enable(this);
        this.body.immovable = true;
        this.body.collideWorldBounds = true;


    } // end _init

    public show(delay):void {
        var sound = this.game.add.audio('wallBounce');
        
        var tween = this.game.add.tween(this).to({alpha: 1}, 800, 'Linear', true, delay );
        tween.onStart.add( ()=>{
            sound.play();
        });
        tween.onComplete.add( ()=>{
            
            SignalManager.instance.dispatch('ready');
        })
    }

}