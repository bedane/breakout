import {GameGroup} from './../util/GameGroup';
import {SignalManager} from './../manager/SignalManager';

export class EndGame extends GameGroup{

    private _box: Phaser.Graphics   = null;
    private _text: Phaser.Text      = null;

    constructor(){
        super();
        this._subscribe();
        this.init();
    }

    private _subscribe(): void {
        SignalManager.instance.add('endGame', this._show, this);
        SignalManager.instance.add('newGame', this._destroy, this);
    }

    private _unsubscribe(): void {
        SignalManager.instance.remove('endGame', this._show, this);
        SignalManager.instance.remove('newGame', this._destroy, this);
    }

    public init(): void {

        // create the square
        var square: Phaser.Graphics = this.game.add.graphics(this.game.world.centerX, 100, this);
        square.lineStyle(4, 0x000000, 1);
        square.drawRect(0, 0, 400, 200);
        square.x = this.game.world.centerX - 200;
        this._box = square;

        // create the logo
        var text: Phaser.Text = this.game.add.text( this.game.world.centerX, 200, 'Breakout', 'Arial Black', this);
        text.fontSize   = 50;
        text.fontWeight = 'bold';
        text.anchor.set(0.5);
        this._text = text;

        // create try again button
        var tryAgain: Phaser.Text = this.game.add.text( this.game.world.centerX, 500, 'Try Again', 'Arial Black', this );
        
        //	Center align
        tryAgain.anchor.set(0.5);
        tryAgain.align = 'center';

        //	Font style
        tryAgain.fontSize = 50;
        tryAgain.fontWeight = 'bold';

        // enable click
        tryAgain.inputEnabled = true;
        tryAgain.events.onInputDown.add(this._buttonClick, this);

        this.alpha = 0;

    }

    private _buttonClick(): void {
        SignalManager.instance.dispatch('newGame');
    }

    private _show(state:string): void {
        var sound = this.game.add.audio('click2');
        sound.play();

        if ( state === 'congrats' ){
            this._text.text = 'Well done';
        } else {
            this._text.text = 'Unlucky';
        }

        this.game.add.tween(this).to({alpha:1}, 800, 'Linear', true, 200);
    }

    private _destroy(): void {
        this.destroy();
        this._unsubscribe();
    }

}