import {GameGroup} from './../util/GameGroup';
import {SignalManager} from './../manager/SignalManager';
import {LifeManager} from './../LifeManager';
import {BlockMap} from './../maps/blockMap';
import {Block} from './../Block';
import {Paddle} from './../Paddle';
import {Ball} from './../Ball';
import {Counter} from './../Counter';

export class MainGame extends GameGroup {

    // public static instance          = null;
    
    private _blockArray: Block[]    = null;
    private _paddle: Paddle         = null;
    private _ball: Ball             = null;
    private _noReadyObjs: number    = 0;
    private _count:number           = 0;
    private _lifeManager: LifeManager = null;

    private _ballSpeed:number       = 220;
    
    constructor() {

        super();

        // if( MainGame.instance === null){
        //     MainGame.instance = this; 
        // }
        this._subscribe();
        this._init();

    }

    /**
     *  @name:          _subscribe
     *  @description:   adds signal listeners used within the game
     *  @param:         null;
     *  @return:        null;
     */
    private _subscribe(): void {
        SignalManager.instance.add('splashDestroy', this._mainGameIntro, this);
        SignalManager.instance.add('ready', this._countIn, this);
        SignalManager.instance.add('gameOn', this._startGame, this);
        SignalManager.instance.add('endGame', this._destroy, this);
    }

    public unsubscribe(): void {
        SignalManager.instance.remove('splashDestroy', this._mainGameIntro, this);
        SignalManager.instance.remove('ready', this._countIn, this);
        SignalManager.instance.remove('gameOn', this._startGame, this);
        SignalManager.instance.remove('endGame', this.destroy, this);
    }

    /**
     *  @name:              _init
     *  @description:       initialise the class
     */
    private _init(): void {

        this.alpha = 0;

        this._blockArray = [];

        // get random map
        var map = BlockMap.instance.getRandomMap();

        for( var i: number = 0; i < map.length; i++ ){
            
            var block: Block = new Block(new Phaser.Point( 100, 20));
            this.add(block);

            block.position.x = map[i][0]
            block.position.y = map[i][1];
            block.alpha = 0;
            this._blockArray.push(block);

        } // end loop

        this._paddle    = new Paddle();
        this.add(this._paddle);
        this._paddle.x = this.game.world.centerX;
        this._paddle.y = 800;

        this._lifeManager = new LifeManager(3);
        this.add(this._lifeManager);
        this._lifeManager.position.x = 500;
        this._lifeManager.position.y = 50;
 
        this._createBall();

        // set the ready count to the number of objects on the main screen
        this._noReadyObjs = this._blockArray.length + 2;

    } // end _init

    /**
     *  @name:      _createBall
     *  @desc:      this creates an instance of new ball
     *  @future:    this could be used to create more balls in furture update
     */
    private _createBall(): void {
        this._ball      = new Ball();
        this._ball.name = 'Ball';
        this.add(this._ball);
        // put in start position
        this._ball.position.x = this.game.world.centerX;
        this._ball.position.y = 600;
        this._ball.pivot.x  = 0.5;
        this._ball.pivot.y  = 0.5;
    }

    /**
     *  @name:      _mainGameIntro
     *  @desc:      tweens for maingame coming into view
     */
    private _mainGameIntro(): void {

        var intro: Phaser.Tween = this.game.add.tween(this).to({alpha:1}, 400, 'Linear', true );
        intro.onComplete.add( () => {
            for( let i:number = 0; i<this._blockArray.length; i++ ){
                
                var block: Block = this._blockArray[i];
                block.show(i * 100);
            
            } // end loop

        });
        
        new Counter(3);        

    } // end _mainGameIntro

    /**
     *  @name:      _countIn
     *  @desc:      simply counts the number of objects that have finished their tween in, so game can commence
     */
    private _countIn(): void {
        this._count++;
        if ( this._count === this._blockArray.length){
            SignalManager.instance.dispatch('startCountdown')
        }
    }

    /**
     *  @name:      _startGame
     *  @desc:      starts the game, move the ball and activate the bricks
     */
    private _startGame(): void {
        this._ball.getGoing();
    } // end _startGame

    /**
     *  @name:      update
     *  @desc:      standard update loop, used to calculate ball hitting things
     */
    update() {

        this._paddle.position.x = this.game.input.x - (this._paddle.width / 2);

       // var t = this;
        this.game.physics.arcade.collide( this._paddle, this._ball, this._ballHitPaddle, null, this );

        for (var i: number = 0; i < this._blockArray.length; i++) {
            
            var b:Block = this._blockArray[i];
            this.game.physics.arcade.collide( b, this._ball, ()=>{
                // change the call to a lamda, weird stuff going on with multiple parameters being passed into the function so easier to do it manualy
                this._ballHitBlock(b, this)
            });
        

        }

        // kill ball
        if( this._ball.y > 900 ) {

            this._ball.destroy();
            
            // remove life from livesManager and check if lives are still available
            var lives = this._lifeManager.lifeLost();
            if ( lives === 0 ){
                SignalManager.instance.dispatch('endGame', 'unlucky');
            } else {
                this._createBall();
                this._ball.show(400);
                new Counter(3);
                SignalManager.instance.dispatch('startCountdown');
            }

        }
    
    }

    /**
     *  @name:          _ballHitBlock
     *  @desc:          play a sound (kinda annoying, sorry), destroy the block that it hits, and removes the block from the block array
     *                  and calls checkEnd ( this method checks the blockarray.length )
     */
    private _ballHitBlock(block: Block, t): void {

        var sound = t.game.add.audio('click3');
        sound.play();

        // remove the block from the block array
        var index:number = t._blockArray.indexOf(block);
        t._blockArray.splice(index, 1);

        block.destroy();
    
        t._checkEnd();

    }
    
    /**
     *  @name:          _ballHitPaddle
     *  @desc:          calls the method to change velocity and angle depending on where the ball hits on the paddle
     */
    private _ballHitPaddle(): void {
        // add sound 
        this._ball.body.velocity.x = this._determineBounceVelocityX(this._paddle, this._ball);
    }

    /**
     * @name:           _determineBounceVelocityX
     * @description:    this method calculates the velocity and angle of the bounce depending on where the ball hits the paddle
     *                  and adds in a small percentage based amount to keep things interesting.
     * @param paddle 
     * @param ball 
     */
    private _determineBounceVelocityX (paddle: Paddle, ball: Ball): number  {
    
        var bounceVelocityX = ball.body.velocity.x;
        
        var ballBodyCenterX = ball.body.x + ball.body.halfWidth;
        var paddleBodyCenterX = paddle.body.x + paddle.body.halfWidth;
        var distanceX = Math.abs(ballBodyCenterX - paddleBodyCenterX);


        if (ball.body.right < paddle.body.x) {
            
            //  Ball is on the left-hand vertical side of the paddle
            var directionVariable = (bounceVelocityX > 0) ? -1 : 1;
            return bounceVelocityX * directionVariable - (distanceX * 2);

        }

        if (ballBodyCenterX == paddleBodyCenterX) {

            //  Ball is perfectly in the middle
            //  Add a little random X to prevent it bouncing straight up!
            return bounceVelocityX + 1 + Math.random() * 8;

        }

        var bounceCoefficient: number = 0;

        if (ballBodyCenterX < paddleBodyCenterX) {

            // Ball hit the top of the paddle, left of center

            bounceCoefficient = -1 * this._coefficient(distanceX, paddle.body.halfWidth);

        } else {

            // Ball hit the paddle right of center, either on the top or on the side

            bounceCoefficient = this._coefficient(paddle.body.halfWidth - distanceX, paddle.body.halfWidth);

        }

        var ratio = (distanceX) / paddle.body.halfWidth * bounceCoefficient;
        
        return (this._ballSpeed * ratio);


    }

    private _coefficient(bounceScale: number, halfWidth): number{
        
        if (bounceScale > 0 && bounceScale < halfWidth / 3) {
            return 0.7;
        } else if (bounceScale > halfWidth / 3 && bounceScale < halfWidth / 3 * 2) {
            return 0.9
        } else {
            //it's at the end make the ball bounce faster
            return 1.1;
        }

    }

    /**
     *  @name:          _checkEnd
     *  @description:   this method checks the blockArray length. if its zero (board cleared), you have won
     */
    private _checkEnd(): void {

        if ( this._blockArray.length === 0 ){
            SignalManager.instance.dispatch('endGame', 'congrats');
        }

    }

    /**
     *  @name:      _destroy
     *  @desc:      used to remove signal and destroy this and all children
     */
    private _destroy(): void {

        this.unsubscribe();
        this.destroy(true);

    }

}