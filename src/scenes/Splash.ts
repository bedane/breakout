import {GameGroup} from './../util/GameGroup';
import {SignalManager} from './../manager/SignalManager';
import {BounceArea} from './../util/BounceArea';

export class Splash extends GameGroup {

    private _button: Phaser.Text    = null;
    private _box: Phaser.Graphics   = null;
    private _text: Phaser.Text      = null; 

    get text():Phaser.Text {
        return this._text;
    }

    constructor() {

        super();
        this._init();
        
    }

    /**
     *  @name:          _init
     *  @description:   initialise the spash group, adding an animated logo and a button
     *  @param:         null
     *  @return:        null
     */
    private _init(): void {

        this._createLogo();

        var text = this.game.add.text( this.game.world.centerX, 500, 'Start', 'Arial Black', this );
        //	Center align
        text.anchor.set(0.5);
        text.align = 'center';

        //	Font style
        text.fontSize = 50;
        text.fontWeight = 'bold';

        // enable click
        text.inputEnabled = true;

        this._button = text;

        text.events.onInputDown.add(this._startGame, this);

    } // end init


    /**
     *  @name:          _createLogo
     *  @description:   creates the animated logo, using text and a graphic. Mimicing the functionality of the game
     *  @param:         null
     *  @returns:       null
     */
    private _createLogo(): void {
        
        // create the square
        var square: Phaser.Graphics = this.game.add.graphics(this.game.world.centerX, 100, this);
        square.lineStyle(4, 0x000000, 1);
        square.drawRect(0, 0, 400, 200);
        square.x = this.game.world.centerX - 200;
        this._box = square;

        // create the logo
        var text: Phaser.Text = this.game.add.text( this.game.world.centerX, 200, 'Breakout', 'Arial Black', this);
        text.fontSize   = 50;
        text.fontWeight = 'bold';
        text.anchor.set(0.5);
        this._text = text;

        this.game.physics.enable([text], Phaser.Physics.ARCADE);
        text.body.velocity.x = this.game.rnd.integerInRange(-50, 50);
        text.body.velocity.y = this.game.rnd.integerInRange(-50, 50);

    } // end _createLogo

    update(){
        new BounceArea( this._text, {
            startX: 120,
            startY: 90,
            endX: 300,
            endY: 250
        });
    }

    private _startGame():void {
        
        this._button.inputEnabled = false;

        var sound = this.game.add.audio('click1');
        sound.play();

        
        // animate splash screen off
        this._animateOut();
    }

    private _animateOut(): void {

        this._button.scale.x = 1;
        this._button.scale.y = 1;
        // depress the start text
        var press = this.game.add.tween(this._button.scale).to({x: 0.9, y: 0.9}, 100, 'Linear', true, 0, 0, true );
        press.onComplete.add(()=>{
            this.game.add.tween(this._button.scale).to({x:0, y:0}, 300, 'Linear', true, 200 )
            this.game.add.tween(this._button).to({alpha: 0}, 400, 'Linear', true, 200 )
        }, this);

        this.game.add.tween(this._box).to({alpha:0}, 400, 'Linear', true, 400 );
        var logoTween: Phaser.Tween = this.game.add.tween(this._text).to({alpha:0}, 400, 'Linear', true, 400 );
        

        logoTween.onComplete.add( ()=>{
            
            this.destroy(true);

            SignalManager.instance.dispatch('splashDestroy');

        }, );
    }

}