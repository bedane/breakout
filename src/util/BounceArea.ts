/**
 *  @name:          BounceArea creates an area where a object bounces around in
 *  @param:         ball: object
 *  @param:         coords: Phaser.Point
 */

 export interface iSurround {
     startX: Number,
     startY: Number,
     endX: Number,
     endY: number
 }

export class BounceArea {

    private _movableObject: Phaser.Sprite | Phaser.Text | Phaser.Graphics = null;
    private _surroundingBox: iSurround = null;

    constructor( obj: Phaser.Sprite | Phaser.Text | Phaser.Graphics, coords: iSurround ) {


        if (obj.body.x < coords.startX){
            obj.body.velocity.x *= -1;
        }
        if (obj.body.x > coords.endX){
            obj.body.velocity.x *= -1;
        }
        if (obj.body.y < coords.startY){
            obj.body.velocity.y *= -1;
        }
        if (obj.body.y > coords.endY){
            obj.body.velocity.y *= -1;
        }

    }

}