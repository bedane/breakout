export class Signal extends Phaser.Signal {

    private _signalID:      string      = null;     // Stores the signal ID of this instance of a signal.
    private _signalActive:  boolean     = false;    // Stores whether or not this signal is active.
    private _signalFired:   boolean     = false;    // Stores whether or not the signal was dispatched.
    private _listening:     any[]       = [];

    /** Gets the ID of the GameSignal.  */
    get ID(): string {
        return this._signalID;
    };
    
    /** Gets the active state of the signal. */
    get inUse(): boolean {
        return this._signalActive;
    };
    
    /** Gets whether or not the signal has been fired. */
    get hasFired(): boolean {
        return this._signalFired;  
    };

    /** 
     * @name:                   GameSignal
     * @description:            Creates a new instance of a game signal.     
     * @params:                 signalName: string                 
     * @return:                 void            
     * */
    constructor(signalName: string) {
        super();
        this._signalID      = signalName;
        this._signalActive  = false;
        this._signalFired   = false;
        this._listening     = [];
    };
        
    /** Overrides add function for Phaser.Signal and sets signalActive to true. */
    public add(listener: Function, listeningContext: any = null, priority: number = 0, ...args: any[]): Phaser.SignalBinding {
        this._signalActive = true;
        this._listening.push(listeningContext);
        return super.add(listener, listeningContext, priority, ...args);  
    };
    
    /** Overrides the remove function for Phaser.Signal and checks for signalActive. */
    public remove(listener: Function, listeningContext: any): Function {
        // Sets signalActive to false if theres no listeners.
        this._listening.splice(this._listening.indexOf(listeningContext), 1);
        if (this.getNumListeners() === 0) {
            this._signalActive = false;
        }
        return super.remove(listener, listeningContext);
    };
    
    public dispatch(...params: any[]): void {
        this._signalFired = true;
        super.dispatch(...params);
    }
    
    public getListeningContexts(): string {
        var retString = '';
        this._listening.forEach(context => {
            var funcNameRegex = /function (.{1,})\(/;
            var results  = (funcNameRegex).exec(context["constructor"].toString());
            var name = (results && results.length > 1) ? results[1] : "";
            retString += name;
            retString += ", ";
        });
        retString += "."
        return retString.replace(', .', '.');
    };
    
}