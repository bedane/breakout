import {bootState} from './states/bootState';
import {mainGameState} from './states/mainGameState';
import {SignalManager} from './manager/SignalManager';

export class Breakout extends Phaser.Game {

    static instance = null;

    constructor() {
	
        super( 640, 960, Phaser.AUTO,""); 
        
        this.state.add('boot', bootState );
        this.state.add('mainGame', mainGameState );

        this.state.start('boot');

        Breakout.instance = this;

        new SignalManager();
        
    }

    resize(){
        console.log('i have been resized');
    }

}
