import {GameGroup} from './util/GameGroup';
import {SignalManager} from './manager/SignalManager';

export class Counter extends GameGroup{

    private _start: number      = 0;
    private _text: Phaser.Text  = null;

    constructor(start:number) {
        super();

        this._start = start;
        this._init();
        this._subscribe();

    }

    private _subscribe(): void {
        SignalManager.instance.add('startCountdown', this._startCountdown, this);
        SignalManager.instance.add('gameOn', this.destroy, this);
    } // end _subscribe

    private _unsubscribe(): void {
        SignalManager.instance.remove('startCountdown', this._startCountdown, this);
        SignalManager.instance.remove('gameOn', this.destroy, this);
    }

    private _init(): void {

        var text: Phaser.Text = this.game.add.text( this.game.world.centerX, this.game.world.centerY, this._start.toString(), 'Arial Black')
        this.add(text);
        this._text = text;
        
        
        text.anchor.x = 0.5;
        text.anchor.y = 0.5;

        text.fontSize = 50;
        this._resetText();

    } // end _init

    private _resetText(): void {
        this._text.alpha      = 0;
        this._text.scale.x    = 0;
        this._text.scale.y    = 0;
    }

    private _reduceCounter(): void {
        this._text.text = this._start.toString();
        this._start--;
    }

    private _startCountdown(): void {

        this._resetText();
        this._reduceCounter();

        this.game.add.tween(this._text).to( {alpha: 1}, 200, 'Linear', true);
        var bounceTween: Phaser.Tween = this.game.add.tween(this._text.scale).to( {x: 1, y: 1}, 400, 'Bounce', true );

        bounceTween.onComplete.add( ()=>{
            if( this._start > 0 ) {
                // reduce the number and hold for 600ms before firing startCountdown again
                this.game.time.events.add( 600, this._startCountdown, this);
            } else {
                SignalManager.instance.dispatch('gameOn');
                this.destroy();
            }
        }, this )

    } // end _startCountdown

    public destroy(): void {
        super.destroy();
        this._unsubscribe();
    }

}