var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {

    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.js', '.json', '.ts']
    },

    entry: {
        app: './src/main.ts'
    },

    plugins: [
        new HtmlWebpackPlugin({
            title: 'Breakout',
            template: 'index.template.ejs'
        }),
        new CopyWebpackPlugin([
            {from:'src/assets',to:'assets'} 
        ]),
    ],

    output: {
        path: 'output',
        filename: 'breakout.js'
    },

    module: {
        loaders: [{
            test: /\.ts$/,
            exclude: /node_modules/,
            loader: 'babel-loader!ts-loader'
        }]
    }
};